"
" This function takes a year and returns 1 if it's a leap year
" and 0 otherwise.
"
function! IsLeap(number) abort
let num = a:number
 if num % 4 == 0 && num % 100 != 0
	return 1
elseif num % 100 == 0
	if num % 400 == 0
		return 1
	else 
		return 0
	endif
else
	return 0
endif	

endfunction
