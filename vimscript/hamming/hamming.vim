"
" This function takes two strings which represent strands and returns
" their Hamming distance.
"
" If the lengths of the strands don't match, throw this exception:
"
"     'The two strands must have the same length.'
"
function! Hamming(strand1, strand2)
       	
  "Check string lenghts first
  if len(a:strand1) != len(a:strand2) 
	  throw 'The two strands must have the same length.'
  endif 
	 let s:count = 0 
	  for i in range(0, len(a:strand1))
		 if(a:strand1[i] != a:strand2[i]) 
			let s:count += 1		
		endif
	endfor	
	return s:count
endfunction
