"
" This function takes any drivel and returns Bob's response.
"
function! Answer(drivel) abort

	"First - empty string
	"Second - yelling
	"Third - Question
	"Fourth - default
	if(a:drivel !~ '\S')	
		return 'Fine. Be that way!'
	elseif(a:drivel =~  '\C[A-Z]' && a:drivel !~ '\C[a-z]')
		return 'Whoa, chill out!'
	elseif(a:drivel[len(a:drivel)-1] == '?')
		return 'Sure.'
	else
		return 'Whatever.'
	endif

endfunction
