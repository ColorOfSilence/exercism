"
" This function takes a DNA strand and returns its RNA complement.
"
"   G -> C
"   C -> G
"   T -> A
"   A -> U
"
" If the input is invalid, return an empty string.
"
" Example:
"
"   :echo ToRna('ACGTGGTCTTAA')
"   UGCACCAGAAUU
"
function! ToRna(strand) abort
	if(a:strand =~# '[^ACGT]')
		return ''
	endif
	let rna = ''
	for i in range(0,len(a:strand))
		if(a:strand[i]==#'G')
			let rna .= 'C'
			continue
		elseif(a:strand[i]==#'C')
			let rna .= 'G'
			continue
		elseif(a:strand[i]==#'T')
			let rna .= 'A'
			continue
		elseif(a:strand[i]==#'A')
			let rna .= 'U'
			continue
		endif
	endfor
	return rna
endfunction
